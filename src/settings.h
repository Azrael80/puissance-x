#ifndef _SETTINGS_H_
#define _SETTINGS_H_
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <time.h>
#include "player.h"

/* Definition des identifiants de paramètres */
enum PARAMS {
    COLS = 0,
    ROWS = 1,
    WIN_DISC = 2,
    GAME_TYPE = 3,
    STARTER = 4,
    PLAYER_NAME_1 = 5,
    PLAYER_NAME_2 = 6,
    SAVE_FILE = 7
};
typedef enum PARAMS PARAM;


/* Interface pour les paramètres de jeu */

/*
    Type de partie :
        0           => Joueur contre Joueur
        1, 2, 3     => Joueur contre Ordinateur
            1       => Débutant
            2       => Intermédiaire
            3       => Difficile
*/

struct SETTINGS {
    char parameters[5]; // Tableau contenant les différents paramètres
    Player players[2]; // Joueurs 
    char *save_file; // Représente le nom du fichier de sauvegarde 
};
typedef struct SETTINGS SETTINGS;

SETTINGS settings;

char **grid; // Grille de jeu

char generateSettings(int argc, char **argv); // Récupère les paramètres de la partie
void initGrid(); // Initialise la grille
void setParam(PARAM param, char val); // Modifie un paramètre 
char getParam(PARAM param); // Récupère un paramètre

#endif