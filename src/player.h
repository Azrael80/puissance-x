
#ifndef _PLAYER_H_
#define _PLAYER_H_
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

/* Header pour les joueurs */

struct Player {
    char id;
    char *name;
    char disc;
    int (*chooseCol)(struct Player);
};
typedef struct Player Player;

/*
*   Créer un joueur (ordinateur ou non)
*   id: identifiant du joueur (0, 1)
*   name: nom du joueur
*   disc: jeton du joueur
*   type: type de joueur
*/
Player createPlayer(char id, char *name, char disc, int type);

int chooseColHuman(Player player); // Demande au joueur de choisir une colonne

#endif