/*
*   Header du système de sauvegarde pour PuissanceX
*/
#ifndef _SAUVEGARDE_H_
#define _SAUVEGARDE_H_
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "settings.h"

#define BUFFER_LENGTH 255

FILE *file;

// Créer une nouvelle sauvegarde (ajoute seulement les paramètres de la partie)
void newSave(); 
void loadSave(); // Charge une sauvegarde
void saveTurn(int col); // Sauvegarde le tour qui vient d'être fait
void makeGame(FILE *file); // Reconstruit la  partie

#endif