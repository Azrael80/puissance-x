#include "puissanceX.h"

char playerTurn = 0; // Défini à qui est le tour

int start(int argc, char **argv) {
    int result = 0;

    if(generateSettings(argc, argv) == 1) {
        startGame();
    } else {
        printf("You have an error in your parameters, the game can't start.");
        result = 1;
    }

    return result;
}

int startGame() {
    char gameStatus = 0;

    // Utilisation du fichier de sauvegarde s'il existe
    if(settings.save_file != NULL) {
        loadSave();
    } else {
        initGrid();
    }

    showPlayersDiscs();
    showGrid();

    // Place le tour sur le bon joueur en fonction des paramètres
    playerTurn = getParam(STARTER);
    

    while(gameStatus == 0) {
        getTurn(settings.players[playerTurn], &gameStatus);
        playerTurn = (playerTurn == 0 ? 1 : 0); // Changement du joueur

        // Affichage de la grille
        showGrid();
    }
    
    // Affichage du résultat de la partie
    showResult(gameStatus);

    return gameStatus;
}

char colFull(int col) {
    //Si la première ligne de la colonne est utilisée, alors la colonne est pleine
    return grid[0][col] != 0 ? 1 : 0;
}

int addDisc(int col, char disc) { 
    char placed = 0;
    int row = 0;
    for(int r = 0; r < getParam(ROWS) && placed == 0; r++) {
        if(grid[r][col] != 0) {
            row = r - 1;
            grid[row][col] = disc;
            placed = 1;
        }
    }
    // Si le jeton n'est pas placé c'est qu'il va dans la dernière ligne
    if(placed == 0) {
        row = getParam(ROWS) - 1;
        grid[row][col] = disc;
    }

    return row;
}

void getTurn(Player player, char *gameStatus) {
    char placed = 0;
    int col;
    while(placed == 0) {

        // Appel de la fonction de l'interface pour choisir une colonne (le nombre de colonne débute à 1)
        col = player.chooseCol(player);
        
        if(col > 0 && col <= getParam(COLS)) {
            if(colFull(col - 1) == 0) {
                int row = addDisc(col - 1, player.disc);
                placed = 1;
                *gameStatus = checkGridRowAndCol(player, row, col - 1);

                // Sauvegarde de la colonne jouée
                saveTurn(col);
            }
            else
                showMessage("Column full.\n");
        }
    }
}

char checkGridRowAndCol(Player player, int row, int col) {
    char disc = player.disc;
    int counter = 0, counter2 = 0;
    char stop_counter = 0, stop_counter2 = 0;
    int r, c;
    int player_number = player.id + 1;
    char cols, rows, win_disc;
    cols = getParam(COLS);
    rows = getParam(ROWS);
    win_disc = getParam(WIN_DISC);

    //Jetons du bas
    for(r = row; r < rows; r++) {
        if(grid[r][col] == disc) {
            counter++;
            if(counter >= win_disc)
                return player_number;
        } else {
            break;
        }
    }

    //Jetons à gauche
    counter = 0;
    for(c = col - 1; c >= 0; c--) {
        if(grid[row][c] == disc)
        {
            counter++;
            // Le joueur gagne si le nombre de jetons à gauche du jeton actuel + 1 
            // est égal au nombre de jetons pour gagner
            if(counter + 1 >= win_disc)
                return player_number;
        } else {
            break;
        }
    }
    
    //Jetons à droite
    for(c = col + 1; c < cols; c++) {
        if(grid[row][c] == disc)
        {
            counter++;
            // Le joueur gagne si le nombre de jetons à gauche et à droite du jeton actuel + 1 
            // est égal au nombre de jetons pour gagner
            if(counter + 1 >= win_disc)
                return player_number;
        } else {
            break;
        }
    }

    //Jetons diagonale gauche à droite
    counter = 0;
    for(int i = 0; i < cols && (stop_counter == 0 || stop_counter2 == 0); i++) {

        //Partie gauche
        r = row - i;
        c = col - i;
        if(stop_counter == 0 && r >= 0 && c >= 0) {
            if(grid[r][c] == disc) {
                counter++;
            } else {
                stop_counter = 1;
            }
        }

        //Partie droite
        r = row + i + 1;
        c = col + i + 1;
        if(stop_counter2 == 0 && r < rows && c < cols) {
            if(grid[r][c] == disc) {
                counter2++;
            } else {
                stop_counter2 = 1;
            }
        }

        if(counter + counter2 >= win_disc)
            return player_number;
    }

    //Jetons diagonale droite à gauche
    counter = counter2 = stop_counter = stop_counter2 = 0;
    for(int i = 0; i < cols && (stop_counter == 0 || stop_counter2 == 0); i++) {

        //Partie gauche
        r = row + i;
        c = col - i;
        if(stop_counter == 0 && r < rows && c >= 0) {
            if(grid[r][c] == disc) {
                counter++;
            } else {
                stop_counter = 1;
            }
        }

        //Partie droite
        r = row - i - 1;
        c = col + i + 1;
        if(stop_counter2 == 0 && r >= 0 && c < cols) {
            if(grid[r][c] == disc) {
                counter2++;
            } else {
                stop_counter2 = 1;
            }
        }

        if(counter + counter2 >= win_disc)
            return player_number;
    }

    // Vérification de grille pleine, retourne 0 si on trouve une case vide.
    for(r = 0; r < rows; r++)
        for(c = 0; c < cols; c++)
            if(grid[r][c] == 0)
                return 0;
    
    // 3 est retourné, car pour arrivé ici, il ne doit pas y avoir de gagnant
    // et la grille doit être pleine.
    return 3;
}