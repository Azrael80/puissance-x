#include "save.h"
#include "puissanceX.h"


void newSave() {
    file = fopen(settings.save_file, "w");
    
    // Sauvegarde des noms des joueurs
    fprintf(file, "%s\n%s\n", settings.players[0].name, settings.players[1].name);

    // Sauvegarde des paramètres
    // On ajoute 1 à la valeur de chaque paramètre
    // pour enregistrer aussi les (char)0
    for(int i = 0; i < 5; i++) {
        fprintf(file, "%c", (settings.parameters[i] + 1));
    }

    fclose(file);
}

void loadSave() {
    if(file = fopen(settings.save_file, "r")) {
        char buffer[BUFFER_LENGTH];
        char c;

        // Le fichier existe, on effectue le chargement
        // Les deux premières lignes sont les noms

        // Nom Joueur 1
        fgets(buffer, BUFFER_LENGTH, file);
        settings.players[0].name = malloc(strlen(buffer));
        strcpy(settings.players[0].name, buffer);
        settings.players[0].name[strlen(buffer) - 1] = 0; // On retire le saut de ligne

        // Nom Joueur 2
        fgets(buffer, BUFFER_LENGTH, file);
        settings.players[1].name = malloc(strlen(buffer));
        strcpy(settings.players[1].name, buffer);
        settings.players[1].name[strlen(buffer) - 1] = 0; // On retire le saut de ligne

        // Création de la partie
        makeGame(file);

        fclose(file);
    } else {
        newSave(); // Nouvelle sauvegarde
        initGrid(); // Initialise la grille
    }
}

void makeGame(FILE *file) {
    int count = 0;
    char c;
    int playerTurn = 0;

    // Lecture de la dernière ligne (paramètres + grille)
    // On récupère les 5 premiers paramètres
    while(count < 5) {
        c = fgetc(file);
        setParam(count, c - 1);
        if(count == STARTER) {
            // Affectation du joueur qui commence
            playerTurn = c - 1;
        }
        count++;
    }

    initGrid();

    // On récupère maintenant les différents tours 
    while((c = fgetc(file)) != EOF) {
        addDisc(c - 1, settings.players[playerTurn].disc);
        playerTurn = playerTurn == 0 ? 1 : 0;
    }

    // La main est redonnée au joueur qui doit jouer
    setParam(STARTER, playerTurn);
}

void saveTurn(int col) {
    if(settings.save_file != NULL) {
        file = fopen(settings.save_file, "a");
        fprintf(file, "%c", col);
        fclose(file);
    }
}