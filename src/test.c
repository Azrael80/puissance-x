#include "player.h"
#include "settings.h"
#include "interface.h"
#include "computer.h"
#include "puissanceX.h"
#include <stddef.h>
#include <string.h>

void testChoixIntermediaire(int cols);
int test2IA(char *name1, char *name2, int (*func1)(Player), int (*func2)(Player), int count);

int main(int argc, char **argv) {
    int result = 0;

    if(generateSettings(argc, argv) == 1) {
        // Test à effectués
        initGrid();
        testChoixIntermediaire(getParam(COLS));
        
        printf("\n\nTest IA : Beginner1(1) VS Beginner2(2):\n");
        test2IA("Beginner1", "Beginner2", chooseColBeginner1, chooseColBeginner2, 10000);
        printf("Resultat attendu : + de victoires pour beginner2\n");

        printf("\n\nTest IA : Beginner2(2) VS Intermediate(3):\n");
        test2IA("Beginner2", "Intermediate", chooseColBeginner2, chooseColIntermediate, 1000);
        printf("Resultat attendu : + de victoires pour Intermediate\n");

        printf("\n\nTest IA : Hard(4) VS Beginner1(1):\n");
        test2IA("Hard", "Beginner1", chooseColHard, chooseColBeginner1, 1000);
        printf("Resultat attendu : + de victoires pour Hard\n");

        printf("\n\nTest IA : Hard(4) VS Beginner2(2):\n");
        test2IA("Hard", "Beginner2", chooseColHard, chooseColBeginner2, 1000);
        printf("Resultat attendu : + de victoires pour Hard\n");

        printf("\n\nTest IA : Hard(4) VS Intermediate(3):\n");
        test2IA("Hard", "Intermediate", chooseColHard, chooseColIntermediate, 1000);
        printf("Resultat attendu : + de victoires pour Hard\n");
    } else {
        printf("You have an error in your parameters, the game can't start.");
        result = 1;
    }

    return result;
}

/*
*   Test algorithme de choix de colonne
*   Répartition pondérée
*   IA: Intermédiaire 
*   Résultat attendu:
*       Nombre plus élevé sur les colonnes du centre
*/
void testChoixIntermediaire(int cols_count) {
    int *cols = (int *)malloc(sizeof(int) * cols_count);
    int col = -1;
    int i = 0;
    Player p = createPlayer(0, "", 0, 2);

    printf("Intervales\n");
    int m = (int)((double)getParam(COLS) * 0.29) + ((double)getParam(COLS) * 0.43);
    printf("[%d, %d]\n", 1, (int)ceil((double)(getParam(COLS)) * 0.29));
    printf("[%d, %d]\n", (int)((double)getParam(COLS) * 0.29), m);
    printf("[%d, %d]\n", m, getParam(COLS));

    for(i = 0; i < cols_count; i++)
        cols[i] = 0;

    for(i = 0; i <= 15000; i++) {
        col = chooseColBeginner2(p);
        cols[col - 1] = cols[col - 1] + 1;
    }

    for(i = 0; i < cols_count; i++) {
        printf("[%d]: %d\n", i + 1, cols[i]);
    }
}

/*
* Fait jouer 2 IA entre elles et retourne l'id du gagnant
*/
int test2IA(char *name1, char *name2, int (*func1)(Player), int (*func2)(Player), int count) {
    Player *player1 = &settings.players[0];
    Player *player2 = &settings.players[1];

    player1->name = name1;
    player1->chooseCol = func1;

    player2->name = name2;
    player2->chooseCol = func2;

    // Lancement de la partie
    int result[3] = {0, 0, 0};
    int winner = 0;

    setParam(STARTER, 0);

    printf("Resultats pour %d parties:\n", count);
    for(int i = 0; i < count; i++) {
        winner = startGame();
        result[winner != 3 ? winner : 0] += 1;
    }
    
    printf("Match(s) nul(s): %d\n", result[0]);
    printf("[%s]: %d victoires.\n", player1->name, result[1]);
    printf("[%s]: %d victoires.\n", player2->name, result[2]);
}