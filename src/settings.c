#include "settings.h"

// TODO : Optimisé cette fonction une fois terminée
char generateSettings(int argc, char **argv) {
    int i = 0;
    char *command;
    int valInteger;
    char *player_1_name = "Joueur 1";
    char *player_2_name = "Joueur 2";

    /* Initialisation des paramètres par défaut */
    setParam(COLS, 7);
    setParam(ROWS, 6);
    setParam(WIN_DISC, 4);
    setParam(GAME_TYPE, 0);
    settings.players[0].disc = 'o';
    settings.players[1].disc = 'x';
    setParam(STARTER, -1);
    settings.save_file = NULL;

    for(i = 1; i < argc; i++) {
        command = argv[i];
        if(strcmp(command, "-p1") == 0) {
            if(i + 1 < argc) { 
                player_1_name = argv[++i];
                if(strlen(player_1_name) > 20) {
                    printf("Max 20 chars for the name\n");
                    return 0;
                }
            } else {
                printf("-p1 <Nom Joueur 1>\n");
                return 0;
            }
        } else if(strcmp(command, "-p2") == 0) {
            if(i + 1 < argc) {
                player_2_name = argv[++i];
                if(strlen(player_2_name) > 20) {
                    printf("Max 20 chars for the name\n");
                    return 0;
                }
            } else {
                printf("-p2 <Nom Joueur 2>\n");
                return 0;
            }
        } else if (strcmp(command, "-c") == 0) {
            if(i + 1 < argc) {
                valInteger = atoi(argv[++i]);
                if(valInteger >= 5 && valInteger <= 10) {
                    setParam(COLS, valInteger);
                } else {
                    printf("-c <5-10>\n");
                    return 0;
                }
            } else {
                printf("-c <5-10>\n");
                return 0;
            }
        } else if (strcmp(command, "-r") == 0) {
            if(i + 1 < argc) {
                valInteger = atoi(argv[++i]);
                if(valInteger >= 5 && valInteger <= 10) {
                    setParam(ROWS, valInteger);
                } else {
                    printf("-r <5-10>\n");
                    return 0;
                }
            } else {
                printf("-r <5-10>\n");
                return 0;
            }
        } else if (strcmp(command, "-d") == 0) {
            if(i + 1 < argc) {
                valInteger = atoi(argv[++i]);
                if(valInteger >= 4 && valInteger <= 7) {
                    setParam(WIN_DISC, valInteger);
                } else {
                    printf("-d <4-7>\n");
                    return 0;
                }
            } else {
                printf("-d <4-7>\n");
                return 0;
            }
        } else if(strcmp(command, "-t") == 0) {
            if(i + 1 < argc) {
                valInteger = atoi(argv[++i]);
                if(valInteger >= 0 && valInteger <= 4) {
                    setParam(GAME_TYPE, valInteger);
                } else {
                    printf("-t <0-4>\n");
                    return 0;
                }
            } else {
                printf("-t <0-3>\n");
                return 0;
            }
        } else if(strcmp(command, "-s") == 0) {
            if(i + 1 < argc) {
                valInteger = atoi(argv[++i]);
                if(valInteger == 0 || valInteger == 1) {
                    setParam(STARTER, valInteger);
                } else {
                    printf("-s <0-1>\n");
                    return 0;
                }
            } else {
                printf("-s <0-1>\n");
                return 0;
            }
        } else if(strcmp(command, "-f") == 0) {
            if(i + 1 < argc) {
                settings.save_file = argv[++i];
            } else {
                printf("-f <file name>\n");
                return 0;
            }
        }
    }

    if(getParam(WIN_DISC) > getParam(COLS) || getParam(WIN_DISC) > getParam(ROWS)) {
        printf("Le nombre de jetons a aligner doit etre plus petit que le nombre de colonnes ou de lignes.");
        return 0;
    }

    // Initialisation des joueurs
    settings.players[0] = createPlayer(0, player_1_name, 'o', 0);
    settings.players[1] = createPlayer(1, player_2_name, 'x', getParam(GAME_TYPE));

    // Le joueur qui commence n'est pas spécifié, il est choisi aléatoirement
    if(getParam(STARTER) == -1) {
        srand(time(NULL));
        setParam(STARTER, rand() % 101 < 50 ? 0 : 1);
    }

    return 1;
}

/*
    Initialisation de la grille de jeu à l'aide du nombre de colonnes.
*/
void initGrid() {
    char rows, cols;
    rows = getParam(ROWS);
    cols = getParam(COLS);
    grid = (char **)malloc(rows * sizeof( char*));
    for(int r = 0; r < rows; r++)
    {
        grid[r] = (char *)malloc(cols * sizeof(char));
        for(int c = 0; c < cols; c++)
            grid[r][c] = 0;
    }
}

void setParam(PARAM param, char val) {
    settings.parameters[param] = val;

    // Si le paramètre est le type de partie
    // le joueur deux doit être modifier
    if(param == GAME_TYPE) {
        settings.players[1] = createPlayer(1, settings.players[1].name, settings.players[1].disc, val);
    }
}

char getParam(PARAM param) {
    return settings.parameters[param];
}