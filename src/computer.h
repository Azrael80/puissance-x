#ifndef _COMPUTER_H_
#define _COMPUTER_H_
#include <time.h>
#include <math.h>
#include "player.h"
#include "settings.h"
#include "alphabeta.h"

// Choix d'une colonne avec l'algo de niveau très facile
int chooseColBeginner1(Player player);

// Choix d'une colonne avec l'algo de niveau facile (choix vers le centre)
int chooseColBeginner2(Player player);

// Choix d'une colonne avec l'algo intermédiaire
int chooseColIntermediate(Player player);

// Choix d'une colonne avec l'algo min-max + elagage alpha beta
int chooseColHard(Player player);

#endif