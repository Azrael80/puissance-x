#include "alphabeta.h"

int minimum(int a, int b) {
    return a > b ? b : a;
}

int maximum(int a, int b) {
    return a >= b ? a : b;
}

char** gridCopy(char **grid, char rows, char cols) {
    char **newGrid = malloc(rows * sizeof(char*));
    int r, c;

    for(r = 0 ; r < rows; r++)
        newGrid[r] = malloc(cols);

    for(int r = 0; r < rows; r++)
        for(int c = 0; c < cols; c++)
            newGrid[r][c] = grid[r][c];

    return newGrid;
}

void freeGrid(char **grid, char rows, char cols) {
    for(int r = 0 ; r < rows; r++)
        free(grid[r]);
    free(grid);
}

int chooseColAlphaBeta(char **grid, char cols, char rows, char max_disc, char min_disc, char win_disc, int depth, int depth_max, int alpha, int beta) {
    int max = 0, m = 0, col = 0;
    char first = 1;
    char **newGrid;

    for(int c = 0; c < cols; c++) {
        if(grid[0][c] == 0) {
            newGrid = addToGrid(grid, c, cols, rows, max_disc);
            m = alphaBeta(newGrid, cols, rows, max_disc, min_disc, win_disc, depth, depth_max, alpha, beta);
            if(m > max || first == 1) {
                max = m;
                col = c;
                first = 0;
            };
            freeGrid(newGrid, rows, cols);
        }
    }

    col = col + 1;

    return col;
}

int alphaBeta(char **grid, char cols, char rows, char max_disc, char min_disc, char win_disc, int depth, int depth_max, int alpha, int beta) {
    int m, nb;
    int status = gridStatus(grid, cols, rows, win_disc);
    char **newGrid;
    if(gridFull(grid, cols) == 1 || status != 0 || depth == depth_max) {
        if(status == max_disc) {
            return 1000000000;
        } else if(status == min_disc) {
            return -1000000000;
        }
        return eval(grid, cols, rows, max_disc, win_disc);
    } else {
        if(depth % 2 == 0) {
            // Noeud max
            m = -10000000;
            nb = cols;
            while(nb != 0 && m < beta) {
                if(grid[0][nb - 1] == 0) {
                    newGrid = addToGrid(grid, nb - 1, cols, rows, max_disc);
                    m = maximum(m, alphaBeta(newGrid, cols, rows, max_disc, min_disc, win_disc, depth + 1, depth_max, alpha, beta));
                    alpha = maximum(alpha, m);
                    freeGrid(newGrid, rows, cols);
                }
                nb = nb - 1;
            }
        } else {
            // Noeud min
            m = 10000000;
            nb = cols;
            while(nb != 0 && m > alpha) {
                if(grid[0][nb - 1] == 0) {
                    newGrid = addToGrid(grid, nb - 1, cols, rows, min_disc);
                    m = minimum(m, alphaBeta(newGrid, cols, rows, max_disc, min_disc, win_disc, depth + 1, depth_max, alpha, beta));
                    beta = minimum(beta, m);
                    freeGrid(newGrid, rows, cols);
                }
                nb = nb - 1;
            }
        }
        return m;
    }
}

char** addToGrid(char **grid, char col, char cols, char rows, char disc) {
    char **newGrid = gridCopy(grid, rows, cols);
    int r;
    for(r = rows - 1; r >= 0; r--) {
        if(newGrid[r][col] == 0) {
            newGrid[r][col] = disc;
            break;
        }
    }
    return newGrid;
}

int gridStatus(char **grid, char cols, char rows, char win_disc) {
    int r, c, r2, c2, d, count;
    char last_disc = 0;

    // Test horizontal
    for(r = 0; r < rows; r++) {
        last_disc = 0;
        count = 1;
        for(c = 0; c < cols; c++) {
            d = grid[r][c];
            if(d != last_disc) count = 1;
            if(d != 0 && d == last_disc) {
                count++;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d;
            }
        }
    }

    // Test vertical
    for(c = 0; c < cols; c++) {
        last_disc = 0;
        count = 1;
        for(r = rows - 1; r >= 0; r--) {
            d = grid[r][c];
            if(d != last_disc) count = 1;
            if(d != 0 && d == last_disc) {
                count++;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d;
            }
        }
    }

    // Test en diagonales
    for(r = 0; r < rows; r++) {
        last_disc = 0;
        count = 1;
        for(r2 = r, c = 0; r2 < rows && c < cols; r2++, c++) {
            d = grid[r2][c];
            if(d != last_disc) count = 1;
            if(d != 0 && d == last_disc) {
                count++;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d;
            }
        }
    }

    for(c = 0; c < cols; c++) {
        last_disc = 0;
        count = 1;
        for(r = 0, c2 = c; r < rows && c2 < cols; r++, c2++) {
            d = grid[r][c2];
            if(d != last_disc) count = 1;
            if(d != 0 && d == last_disc) {
                count++;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d;
            }
        }
    }

    for(r = 0; r < rows; r++) {
        last_disc = 0;
        count = 1;
        for(r2 = r, c = cols - 1; r2 < rows && c >= 0; r2++, c--) {
            d = grid[r2][c];
            if(d != last_disc) count = 1;
            if(d != 0 && d == last_disc) {
                count++;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d;
            }
        }
    }

    for(c = cols -1; c >= 0; c--) {
        last_disc = 0;
        count = 1;
        for(r = 0, c2 = c; r < rows && c2 >= 0; r++, c2--) {
            d = grid[r][c2];
            if(d != last_disc) count = 1;
            if(d != 0 && d == last_disc) {
                count++;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d;
            }
        }
    }

    return 0;
}

int restCols(char **grid, char cols) {
    int restCols = cols; // Nombre de colonnes qu'il reste
    for(int c = 0; c < cols; c++) {
        // Si le premier jeton de la colonne n'est pas 0, alors la colonne est pleine
        if(grid[0][c] != 0) {
            restCols -= 1;
        }
    }
    return restCols;
}

int gridFull(char **grid, char cols) {
    return restCols(grid, cols) > 0 ? 0 : 1;
}

int eval(char **grid, char cols, char rows, char disc, char win_disc) {
    int r, c, r2, c2, d, count;
    char last_disc = 0;
    int val = 0;

    // Test horizontal
    for(r = 0; r < rows; r++) {
        last_disc = 0;
        count = 1;
        for(c = 0; c < cols; c++) {
            d = grid[r][c];
            if(d != last_disc) count = 1;
            if(d != 0) {
                if(d == last_disc) count++;
                val += d == disc ? (5 ^ count) : -(5 ^ count);
            } else if(last_disc != 0) {
                val += last_disc == disc ? 1 : -1;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d == disc ? 1000000000 : -1000000000;
            }
        }
    }

    // Test vertical
    for(c = 0; c < cols; c++) {
        last_disc = 0;
        count = 1;
        for(r = rows - 1; r >= 0; r--) {
            d = grid[r][c];
            if(d != last_disc) count = 1;
            if(d != 0) {
                if(d == last_disc) count++;
                val += d == disc ? (5 ^ count) : -(5 ^ count);
            } else if(last_disc != 0) {
                val += last_disc == disc ? 1 : -1;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d == disc ? 1000000000 : -1000000000;
            }
        }
    }

    // Test en diagonales
    for(r = 0; r < rows; r++) {
        last_disc = 0;
        count = 1;
        for(r2 = r, c = 0; r2 < rows && c < cols; r2++, c++) {
            d = grid[r2][c];
            if(d != last_disc) count = 1;
            if(d != 0) {
                if(d == last_disc) count++;
                val += d == disc ? (5 ^ count) : -(5 ^ count);
            } else if(last_disc != 0) {
                val += last_disc == disc ? 1 : -1;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d == disc ? 1000000000 : -1000000000;
            }
        }
    }

    for(c = 0; c < cols; c++) {
        last_disc = 0;
        count = 1;
        for(r = 0, c2 = c; r < rows && c2 < cols; r++, c2++) {
            d = grid[r][c2];
            if(d != last_disc) count = 1;
            if(d != 0) {
                if(d == last_disc) count++;
                val += d == disc ? (5 ^ count) : -(5 ^ count);
            } else if(last_disc != 0) {
                val += last_disc == disc ? 1 : -1;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d == disc ? 1000000000 : -1000000000;
            }
        }
    }
    
    for(r = 0; r < rows; r++) {
        last_disc = 0;
        count = 1;
        for(r2 = r, c = cols - 1; r2 < rows && c >= 0; r2++, c--) {
            d = grid[r2][c];
            if(d != last_disc) count = 1;
            if(d != 0) {
                if(d == last_disc) count++;
                val += d == disc ? (5 ^ count) : -(5 ^ count);
            } else if(last_disc != 0) {
                val += last_disc == disc ? 1 : -1;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d == disc ? 1000000000 : -1000000000;
            }
        }
    }

    for(c = cols -1; c >= 0; c--) {
        last_disc = 0;
        count = 1;
        for(r = 0, c2 = c; r < rows && c2 >= 0; r++, c2--) {
            d = grid[r][c2];
            if(d != last_disc) count = 1;
            if(d != 0) {
                if(d == last_disc) count++;
                val += d == disc ? (5 ^ count) : -(5 ^ count);
            } else if(last_disc != 0) {
                val += last_disc == disc ? 1 : -1;
            }
            last_disc = d;
            if(count >= win_disc) {
                return d == disc ? 1000000000 : -1000000000;
            }
        }
    }

    return val;
}