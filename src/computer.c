#include "computer.h"

int randomNumber(int min, int max)
{
   return min + rand() % (max + 1 - min);
}

int chooseColBeginner1(Player player) {
    // Retourne une colonne au hasard
    int col = -1;
    do {
        col = randomNumber(1, getParam(COLS));
    } while(grid[0][col - 1] != 0);
    return col;
}

int chooseColBeginner2(Player player) {
    // Retourne une colonne au hasard (en visant plus ou moins le centre de la grille)
    int random = 0;
    int col = -1;
    int middle1 = (int)ceil((double)getParam(COLS) * 0.29);
    int middle2 = (int)((double)getParam(COLS) * 0.29) + ((double)getParam(COLS) * 0.43);
    do {
        random = randomNumber(0, 10000);
        if(random < 2500) {
            col = randomNumber(1, middle1);
        } else if(random < 7500) {
            col = randomNumber(middle1, middle2);
        } else {
            col = randomNumber(middle2, getParam(COLS));
        }
    } while(grid[0][col - 1] != 0);
    return col;
}

int chooseColIntermediate(Player player) {
    int col = -1;
    int random = 0;
    random = randomNumber(0, 10000);

    // 1x sur 3 on fait appel à alphabeta

    if(random > 6666) {
        col = chooseColBeginner2(player);
    } else {
        col = chooseColHard(player);
    }

    return col;
}

int chooseColHard(Player player) {
    return chooseColAlphaBeta(grid, getParam(COLS), getParam(ROWS), player.disc, player.disc == 'o' ? 'x' : 'o',
    getParam(WIN_DISC), 1, 8, -1000000, 1000000);
}