#ifndef OS_TYPE
#define OS_TYPE 0 // 0 = windows, 1 = LINUX
#endif

#ifndef _INTERFACE_H_
#define _INTERFACE_H_
#include <stdio.h>
#include <ctype.h>
#include "player.h"
#include "settings.h"

/*
*   Fonction: showMessage
*   Paramètres:
*       message: le message a affiché
*   Description:
*       Affiche un message
*/
void showMessage(char *message);

/*
*   Fonction: showPlayersDiscs
*   Description:
*       Affiche pour chaque joueur le jeton qu'il utilise
*/
void showPlayersDiscs(); 

/*
*   Fonction: showGrid
*   Description:
*       Affiche la grille de jeu
*/
void showGrid();

/*
*   Fonction: askTurn
*   Paramètres:
*       player: le joueur
*   Description:
*       Demande au joueur de choisir une colonne
*/
int askTurn(Player player);

/*
*   Fonction: showResult
*   Paramètres:
*       result: le résultat de la partie
*   Description:
*       Affiche le résultat de la partie
*/
void showResult(int result);

#endif