#include "interface.h"

void showMessage(char *message) {
    printf("%s", message);
}

void showPlayersDiscs() {
    char a[50];
    for(int p = 0; p < 2; p++) {
        printf("%s : %c\n", settings.players[p].name, settings.players[p].disc);
    }

    do {
        printf("Start the game y/n ? ");
        scanf("%s", a);
    } while(strcmp(a, "y") != 0 && strcmp(a, "Y") != 0);
}

void showGrid() {
    system(OS_TYPE == 0 ? "cls" : "clear");
    int c = 0;
    int r = 0;
    char rows, cols;
    cols = getParam(COLS);
    rows = getParam(ROWS);
    printf("\n\n");
    printf("  1");
    for(c = 1; c < cols; c++)
        printf("   %d", c + 1);
    printf("\n");
    for(r = 0; r < rows; r++) {
        printf("|");
        for(c = 0; c < cols; c++) {
            printf(" %c |", (grid[r][c] == 0 ? ' ' : grid[r][c]));
        }
        printf("\n");
    }
    printf("\n");
}

int askTurn(Player player) {
    int col = -1, response;
    printf("%s (%c) (1", player.name, player.disc);
    for(int i = 1; i < getParam(COLS); i++)
        printf(", %d", i + 1);
    printf(") ? ");

    response = scanf("%d", &col);

    if(response == EOF) {
        col = -1;
    }

    return col;
}

void showResult(int result) {
    switch(result) {
        case 3: // Partie nulle
            printf("Match nul\n");
        break;
        default:
            printf("%s win !\n", settings.players[result - 1].name);
        break;
    }
}