#ifndef _ALPHABETA_H_
#define _ALPHABETA_H_
#include <stdlib.h>

int minimum(int a, int b); // Retourne le nombre le plus petit entre a et b
int maximum(int a, int b); // Retourne le nombre le plus grand entre a et b

/*
 *   Fonction: gidCopy
 *   Paramètres:
 *       grid: grille à copier
 *       rows: nombre de lignes de la grille
 *       cols: nombre de colonnes de la grille
 *   Sortie:
 *        Copie de la grille
 *    
 *    Description : Copie la grille
 */
char** gridCopy(char **grid, char rows, char cols);

/*
 *   Fonction: freeGrid
 *   Paramètres:
 *       grid: grille à supprimer
 *       rows: nombre de lignes de la grille
 *       cols: nombre de colonnes de la grille
 *    
 *    Description : Libère l'espace utilisé par la grille
 */
void freeGrid(char **grid, char rows, char cols);

/*
 *   Fonction: chooseColAlphaBeta
 *   Paramètres:
 *       grid: grille de jeu
 *       cols: nombre de colonnes de la grille
 *       rows: nombre de lignes de la grille
 *       max_disc: jeton du joueur max (joueur actuel)
 *       min_disc: jeton du joueur min (adversaire)
 *       win_disc: nombre de jetons à aligner pour gagner
 *       depth: profondeur actuelle
 *       depth_max: profondeur maximale
 *       alpha: valeur alpha
 *       beta: valeur beta
 *    Sortie: Entier
 *
 *    Description : 
 *       Retourne la colonne que le joueur doit choisir, en utilisant l'algorithme min-max (alphabeta)
 */
int chooseColAlphaBeta(char **grid, char cols, char rows, char max_disc, char min_disc, char win_disc, int depth, int depth_max, int alpha, int beta);

/*
 *   Fonction: alphaBeta
 *   Paramètres:
 *       grid: grille de jeu
 *       cols: nombre de colonnes de la grille
 *       rows: nombre de lignes de la grille
 *       max_disc: jeton du joueur max (joueur actuel)
 *       min_disc: jeton du joueur min (adversaire)
 *       win_disc: nombre de jetons à aligner pour gagner
 *       depth: profondeur actuelle
 *       depth_max: profondeur maximale
 *       alpha: valeur alpha
 *       beta: valeur beta
 *    Sortie: Entier
 *
 *    Description :
 *       Implémentation de l'algorithme min-max avec élagage alpha-bêta
 *       Retourne la valeur actuelle de la grille de jeu, calculée à l'aide de l'algorithme min-max avec élagage alpha-beta
 */
int alphaBeta(char **grid, char cols, char rows, char max_disc, char min_disc, char win_disc, int depth, int depth_max, int alpha, int beta);

/*
 *   Fonction: addToGrid
 *   Paramètres:
 *       grid: grille de jeu
 *       col: colonne où il faut ajouter le jeton
 *       cols: nombre de colonnes de la grille
 *       rows: nombre de lignes de la grille
 *       disc: jeton à ajouter
 *    Sortie: char[][]
 *
 *    Description :
 *       Créer une nouvelle grille en plaçant le jeton dans la colonne choisie.
 */
char** addToGrid(char **grid, char col, char cols, char rows, char disc);

/*
 *   Fonction: gridStatus
 *   Paramètres:
 *       grid: grille de jeu
 *       cols: nombre de colonnes de la grille
 *       rows: nombre de lignes de la grille
 *       win_disc: nombre de jetons à aligner
 *    Sortie: entier
 *
 *    Description :
 *       Retourne 0 s'il n'y a pas de gagnant, sinon retourne le jeton du gagnant
 */
int gridStatus(char **grid, char cols, char rows, char win_disc);

/*
 *   Fonction: restCols
 *   Paramètres:
 *       grid: grille à analyser
 *       cols: nombre de colonnes de la grille
 *   Sortie: Entier
 *    
 *    Description : Retourne le nombre de colonnes pouvant être jouées
 */
int restCols(char **grid, char cols);

/*
 *   Fonction: gridFull
 *   Paramètres:
 *       grid: grille de jeu
 *       cols: nombre de colonnes de la grille
 *    Sortie: entier
 *
 *    Description :
 *       Retourne 0 si la grille n'est pas pleine, sinon retourne 1.
 */
int gridFull(char **grid, char cols);

/*
 *   Fonction: gridFull
 *   Paramètres:
 *       grid: grille de jeu
 *       cols: nombre de colonnes de la grille
 *       rows: nombre de lignes de la grille
 *       disc: jeton du joueur actuel
 *       win_disc: nombre de jetons à aligner
 *    Sortie: entier
 *
 *    Description :
 *       Retourne une valeur représentant l'évaluation de la grille, plus la valeur est
 *       haute, plus la grille est bonne pour le joueur, plus la valeur est basse, 
 *       moins elle l'est.
 */
int eval(char **grid, char cols, char rows, char disc, char win_disc);

#endif