#include "interface.h"

void showMessage(char *message) {
    printf("%s", message);
}

void showPlayersDiscs() {

}

void showGrid() {
    // N'affiche pas la grille pour les tests
}

int askTurn(Player player) {
    int col = -1, response;
    printf("%s (%c) (1", player.name, player.disc);
    for(int i = 1; i < getParam(COLS); i++)
        printf(", %d", i + 1);
    printf(") ? ");

    response = scanf("%d", &col);

    if(response == EOF) {
        col = -1;
    }

    return col;
}

void showResult(int result) {

}