#include "player.h"
#include "interface.h"
#include "settings.h"
#include "computer.h"

Player createPlayer(char id, char *name, char disc, int type) {
    Player player;
    time_t t;

    player.id = id;
    player.name = name;
    player.disc = disc;

    srand((unsigned)time(&t));

    // Choisit la bonne fonction pour demandé la colonne à joué
    // type > 0 => Ordinateur
    // type = 0 => Joueur humain
    switch (type)
    {
        case 1: // Niveau très facile
            player.chooseCol = &chooseColBeginner1;
            break;
        case 2: // Niveau facile
            player.chooseCol = &chooseColBeginner2;
            break;
        case 3: // Niveau intermédiaire
            player.chooseCol = &chooseColIntermediate;
            break;
        case 4: // Niveau difficile
            player.chooseCol = &chooseColHard;
            break;
        default: // Humain
            player.chooseCol = &chooseColHuman;
            break;
    }

    return player;
}

int chooseColHuman(Player player) {
    // Appel de la fonction de l'interface
    return askTurn(player);
}