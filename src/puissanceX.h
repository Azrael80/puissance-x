#ifndef _PUISSANCEX_H_
#define _PUISSANCEX_H_
#include "player.h"
#include "settings.h"
#include "interface.h"
#include "save.h"

int start(int argc, char **argv); // Démarre le jeu avec les paramètres de démarage
int startGame(); // Démarre la partie et retourne le résultat (nul ou joueur gagnant)

/*
 *   Fonction: getTurn
 *   Paramètres:
 *       player: représente le joueur qui doit jouer son tour
 *       gameStatus: représente le status du tour (grille pleine, ou tour gagné)
 */
void getTurn(Player player, char *gameStatus);

/*
 *   Fonction: colFull
 *   Paramètres:
 *       col: colonne testée
 *   Retourne 1 ou 0 si la colonne est pleine ou non
 */
char colFull(int col);

/*
 *   Fonction: addDisc
 *   Paramètres:
 *       col: colonne choisie pour placer le jeton
 *       disc: le jeton
 *   Retourne la ligne où a été placer le jeton
 */
int addDisc(int col, char disc);

/*
 *   Fonction: checkGridRowAndCol
 *   Paramètres:
 *       player: joueur ayant posé le jeton
 *       row: ligne du jeton posé
 *       col: colonne du jeton posé
 *   Retourne 0 si la partie continue, 1/2 si un joueur a gagné, 3 pour match nul.
 *
 *   Description:
 *       Lorsqu'un joueur place un jeton, on calcul les différents alignements
 *       à partir de la position du jeton posé afin de savoir si le coup
 *       est gagnant.
 */
char checkGridRowAndCol(Player player, int row, int col); 
#endif